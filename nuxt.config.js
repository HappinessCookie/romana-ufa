module.exports = {
	/*
	 ** Headers of the page
	 */
	head: {
		title: 'Установка детских игровых компании Romana по Республике Башкортостан',
		meta: [
			{ charset: 'utf-8' },
			{
				'http-equiv': 'X-UA-Compatible',
				content: 'IE=edge'
			},
			{
				name: 'viewport',
				content: 'width=device-width, initial-scale=1'
			},
			{
				name: 'description',
				content: 'Установка детских игровых и спортивных площадок компании Romana по Республике Башкортостан'
			},
			{
				name: 'keywords',
				content: ''
			},
			{
				name: 'msapplication-tap-highlight',
				content: 'no'
			},
			{
				name: 'mobile-web-app-capable',
				content: 'yes'
			},
			{
				name: 'application-name',
				content: 'Установка детских игровых компании Romana по Республике Башкортостан'
			},
			{
				name: 'apple-mobile-web-app-capable',
				content: 'yes'
			},
			{
				name: 'apple-mobile-web-app-status-bar-style',
				content: '#4e69c4'
			},
			{
				name: 'theme-color',
				content: '#4e69c4'
			},
			{
				name: 'yandex-verification',
				content: '27b62fa4be7a9cb0'
			}
		],
		link: [
			{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
		]
	},
	/*
	 ** Customize the progress-bar color
	 */
	loading: false,
	/*
	 ** Build configuration
	 */
	build: {},
	plugins: ['~plugins/vue-resource', '~plugins/vue-mask']
}
