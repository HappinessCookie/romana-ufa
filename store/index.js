import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex);

const state = {
	currentPage: 1,
	itemPerPage: 8,
	currentGroup: 1,
	products: [],
	groups: [
		{ id: 1, name: 'Дачные комплексы', catalogUrl: 'catalog2.json' },
		{ id: 2, name: 'Домашние комплексы', catalogUrl: 'catalog1.json' },
		{ id: 3, name: 'Мягкие модули', catalogUrl: 'catalog3.json' },
		{ id: 4, name: 'Игровое оборудование', catalogUrl: 'catalog4.json' },
		{ id: 5, name: 'Спортивное оборудование', catalogUrl: 'catalog5.json' },
		{ id: 6, name: 'Малые архитектурные формы', catalogUrl: 'catalog6.json' }
	],
	detailProduct: {
		show: false,
		content: null
	},
	modalImage: {
		show: false,
		image: null
	},
	alert: {
		show: false,
		content: null
	},
	cart: {
		show: false,
		list: {}
	},
	cartRequest: {
		show: false
	}
};

const mutations = {
	SET_PAGE (state, payload) {
		state.currentPage = payload;
	},
	SET_GROUP (state, payload) {
		state.currentGroup = payload;
	},
	SET_PRODUCTS (state, payload) {
		state.products = payload;
	},
	SET_DETAIL_PRODUCT (state, payload) {
		state.detailProduct.content = payload;
	},
	SHOW_DETAIL_PRODUCT (state) {
		state.detailProduct.show = true;
	},
	HIDE_DETAIL_PRODUCT (state) {
		state.detailProduct.show = false;
	},
	SET_MODAL_IMAGE (state, payload) {
		state.modalImage.image = payload;
	},
	SHOW_MODAL_IMAGE (state) {
		state.modalImage.show = true;
	},
	HIDE_MODAL_IMAGE (state) {
		state.modalImage.show = false;
	},
	CHANGE_ALERT_CONTENT (state, payload) {
		state.alert.content = payload;
	},
	SHOW_ALERT (state) {
		state.alert.show = true;
	},
	HIDE_ALERT (state) {
		state.alert.show = false;
	},
	CART_ADD_PRODUCT (state, payload) {
		state.cart.list = { ...state.cart.list, [payload.id]: payload };
	},
	CART_UPDATE_PRODUCT (state, payload) {
		state.cart.list = { ...state.cart.list, [payload.id]: payload };
	},
	CART_REMOVE_PRODUCT (state, payload) {
		state.cart.list = payload;
	},
	SHOW_CART_DETAIL (state) {
		state.cart.show = true;
	},
	HIDE_CART_DETAIL (state) {
		state.cart.show = false;
	},
	SHOW_CART_REQUEST (state) {
		state.cartRequest.show = true;
	},
	HIDE_CART_REQUEST (state) {
		state.cartRequest.show = false;
	}
};

const actions = {
	changePage ({ commit }, payload) {
		if (!Number.isInteger(payload)) {
			return false;
		}
		commit('SET_PAGE', payload);
	},
	changeGroup ({ commit }, payload) {
		commit('SET_GROUP', payload.id);
		commit('SET_PAGE', 1);
		Vue.http.get(payload.catalogUrl).then(response => {
			commit('SET_PRODUCTS', response.body);
		}, response => {
		});
	},
	changeDetailProduct ({ commit }, payload) {
		commit('SET_DETAIL_PRODUCT', payload);
	},
	resetDetailProduct ({ commit }) {
		commit('SET_DETAIL_PRODUCT', null);
	},
	showDetailProduct ({ commit }) {
		commit('SHOW_DETAIL_PRODUCT');
	},
	hideDetailProduct ({ commit }) {
		commit('HIDE_DETAIL_PRODUCT');
	},
	changeModalImage ({ commit }, payload) {
		commit('SET_MODAL_IMAGE', payload.image);
	},
	showModalImage ({ commit }) {
		commit('SHOW_MODAL_IMAGE');
	},
	hideModalImage ({ commit }) {
		commit('HIDE_MODAL_IMAGE');
	},
	showAlert ({ commit }, payload) {
		commit('CHANGE_ALERT_CONTENT', payload);
		commit('SHOW_ALERT');
	},
	hideAlert ({ commit }) {
		commit('HIDE_ALERT');
	},
	cartAddProduct ({ commit }, payload) {
		let result = payload;
		result.count = '1';
		commit('CART_ADD_PRODUCT', result);
	},
	cartUpdateProduct ({ commit }, payload) {
		commit('CART_UPDATE_PRODUCT', payload);
	},
	cartRemoveProduct ({ commit, state }, payload) {
		Vue.delete(state.cart.list, payload.id);
		commit('CART_REMOVE_PRODUCT', state.cart.list);
	},
	showCartDetail ({ commit }) {
		commit('SHOW_CART_DETAIL');
	},
	hideCartDetail ({ commit }) {
		commit('HIDE_CART_DETAIL');
	},
	showCartRequest ({ commit }) {
		commit('SHOW_CART_REQUEST');
	},
	hideCartRequest ({ commit }) {
		commit('HIDE_CART_REQUEST');
	}
};

const getters = {
	productsOnPage: (state) => {
		let start = state.currentPage * state.itemPerPage - state.itemPerPage;
		let end = start + state.itemPerPage;
		return state.products.slice(start, end);
	}
};

const store = () => new Vuex.Store({
	state,
	mutations,
	actions,
	getters
});

export default store